local handlers = require('asdfakl.lsp.handlers')
local settings = require('asdfakl.lsp.settings')
handlers.setup()

local lspconfig = require('lspconfig')
local dev_env = vim.env.DEV_ENV or 'home'

local defaults = {
  on_attach = handlers.on_attach,
  capabilities = handlers.capabilities,
  flags = {
    debounce_text_changes = 150,
  }
}

local servers = {
  'gopls',
  'terraformls',
  'lua_ls',
}

if dev_env == 'work' then
  local configs = require('lspconfig.configs')

  local token = vim.env.SNYK_TOKEN
  assert(token, 'environment variable SNYK_TOKEN required')

  configs.snyk = configs.snyk or {
    default_config = {
      cmd = {'snyk-ls'},
      filetypes = {'go', 'gomod', 'terraform'},
      root_dir = function(name)
        return lspconfig.util.find_git_ancestor(name) or vim.loop.os_homedir()
      end,
      init_options = {
        activateSnykCode = 'true',
        activateSnykIac = 'false',
        cliPath = vim.env.HOME .. '/.local/bin/snyk',
        enableTrustedFoldersFeature = 'false',
        endpoint = 'https://app.eu.snyk.io/api',
        token = token,
      },
    },
  }

  -- table.insert(servers, 'snyk')
end

for _, server in ipairs(servers) do
  lspconfig[server].setup(vim.tbl_extend('error', defaults, settings.get(server)))
end
