local M = {}

local actions = require('telescope.actions')
local actions_state = require('telescope.actions.state')
local builtin = require('telescope.builtin')
local finders = require('telescope.finders')
local pickers = require('telescope.pickers')
local themes = require('telescope.themes')
local config = require('telescope.config').values

--[[
client.server_capabilities = {
  callHierarchyProvider = true,
  codeActionProvider = {
    codeActionKinds = { "quickfix", "refactor.extract", "refactor.rewrite", "source.fixAll", "source.organizeImports" }
  },
  codeLensProvider = vim.empty_dict(),
  completionProvider = {
    completionItem = vim.empty_dict(),
    triggerCharacters = { "." }
  },
  definitionProvider = true,
  documentFormattingProvider = true,
  documentHighlightProvider = true,
  documentLinkProvider = vim.empty_dict(),
  documentOnTypeFormattingProvider = {
    firstTriggerCharacter = ""
  },
  documentSymbolProvider = true,
  executeCommandProvider = {
    commands = { "gopls.add_dependency", "gopls.add_import", "gopls.apply_fix", "gopls.check_upgrades", "gopls.edit_go_directive", "gopls.gc_details", "gopls.generate", "gopls.generate_gopls_mod", "gopls.go_get_package", "gopls.list_imports", "gopls.list_known_packages", "g
opls.regenerate_cgo", "gopls.remove_dependency", "gopls.run_tests", "gopls.run_vulncheck_exp", "gopls.start_debugging", "gopls.test", "gopls.tidy", "gopls.toggle_gc_details", "gopls.update_go_sum", "gopls.upgrade_dependency", "gopls.vendor" }
  },
  foldingRangeProvider = true,
  hoverProvider = true,
  implementationProvider = true,
  inlayHintProvider = vim.empty_dict(),
  referencesProvider = true,
  renameProvider = {
    prepareProvider = true
  },
  signatureHelpProvider = {
    triggerCharacters = { "(", "," }
  },
  textDocumentSync = {
    change = 2,
    openClose = true,
    save = vim.empty_dict()
  },
  typeDefinitionProvider = true,
  workspace = {
    workspaceFolders = {
      changeNotifications = "workspace/didChangeWorkspaceFolders",
      supported = true
    }
  },
  workspaceSymbolProvider = true
}
]] --

M.show = function(client)
  local caps = client.server_capabilities

  local entries = {
    {
      cap = caps.signatureHelpProvider,
      display = 'Signature Help',
      action = vim.lsp.buf.signature_help,
    },
    {
      cap = caps.referencesProvider,
      display = 'References',
      action = builtin.lsp_references,
    },
    {
      cap = caps.documentSymbolProvider,
      display = 'Document Symbols',
      action = builtin.lsp_document_symbols,
    },
    {
      cap = caps.codeActionProvider,
      display = 'Code Actions',
      action = vim.lsp.buf.code_action,
    },
    {
      cap = caps.renameProvider,
      display = 'Rename',
      action = vim.lsp.buf.rename,
    },
    {
      cap = caps.implementationProvider,
      display = 'Implementations',
      action = builtin.lsp_implementations,
    },
    {
      cap = caps.typeDefinitionProvider,
      display = 'Type Definitions',
      action = builtin.lsp_type_definitions,
    },
    {
      cap = true,
      display = 'Diagnostic Quickfix List',
      action = vim.diagnostic.setqflist,
    },
  }

  local resolved_entries = {}

  for _, entry in ipairs(entries) do
    if entry.cap then
      resolved_entries[#resolved_entries + 1] = entry
    end
  end

  pickers.new(themes.get_dropdown(), {
    prompt_title = 'LSP Context Menu',
    finder = finders.new_table({
      results = resolved_entries,
      entry_maker = function(entry)
        return {
          value = entry,
          display = entry.display,
          ordinal = entry.display,
        }
      end,
    }),
    sorter = config.generic_sorter(nil),
    attach_mappings = function(prompt_bufnr, _)
      actions.select_default:replace(function()
        actions.close(prompt_bufnr)
        local selection = actions_state.get_selected_entry()
        selection.value.action()
      end)
      return true
    end,
  }):find()
end

return M
