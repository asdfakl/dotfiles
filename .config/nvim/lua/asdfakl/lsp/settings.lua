local M = {}

local dev_home = vim.env.DEV_HOME or vim.env.HOME
assert(dev_home, 'environment variable DEV_HOME or HOME required')

local luals_root_path = dev_home .. '/github.com/LuaLS/lua-language-server'
local luals_binary = luals_root_path .. '/bin/lua-language-server'

local settings = {
  lua_ls = {
    cmd = {
      luals_binary,
      '-E',
      luals_root_path .. '/main.lua',
    },
    settings = {
      Lua = {
        diagnostics = {
          globals = { "vim" },
        },
        workspace = {
          library = {
            [vim.fn.expand("$VIMRUNTIME/lua")] = true,
            [vim.fn.stdpath("config") .. "/lua"] = true,
          },
        },
      },
    },
  },
  gopls = {
    settings = {
      gopls = {
        buildFlags = { "-tags=integration" },
      },
    },
  },
  terraformls = {
    filetypes = {'terraform'}, -- workaround for https://github.com/neovim/neovim/issues/23184
  },
}

M.get = function(lsp_server)
  return settings[lsp_server] or {}
end

return M
