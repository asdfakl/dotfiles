local M = {}

--[[
client.server_capabilities = {
  callHierarchyProvider = true,
  codeActionProvider = {
    codeActionKinds = { "quickfix", "refactor.extract", "refactor.rewrite", "source.fixAll", "source.organizeImports" }
  },
  codeLensProvider = vim.empty_dict(),
  completionProvider = {
    completionItem = vim.empty_dict(),
    triggerCharacters = { "." }
  },
  definitionProvider = true,
  documentFormattingProvider = true,
  documentHighlightProvider = true,
  documentLinkProvider = vim.empty_dict(),
  documentOnTypeFormattingProvider = {
    firstTriggerCharacter = ""
  },
  documentSymbolProvider = true,
  executeCommandProvider = {
    commands = { "gopls.add_dependency", "gopls.add_import", "gopls.apply_fix", "gopls.check_upgrades", "gopls.edit_go_directive", "gopls.gc_details", "gopls.generate", "gopls.generate_gopls_mod", "gopls.go_get_package", "gopls.list_imports", "gopls.list_known_packages", "g
opls.regenerate_cgo", "gopls.remove_dependency", "gopls.run_tests", "gopls.run_vulncheck_exp", "gopls.start_debugging", "gopls.test", "gopls.tidy", "gopls.toggle_gc_details", "gopls.update_go_sum", "gopls.upgrade_dependency", "gopls.vendor" }
  },
  foldingRangeProvider = true,
  hoverProvider = true,
  implementationProvider = true,
  inlayHintProvider = vim.empty_dict(),
  referencesProvider = true,
  renameProvider = {
    prepareProvider = true
  },
  signatureHelpProvider = {
    triggerCharacters = { "(", "," }
  },
  textDocumentSync = {
    change = 2,
    openClose = true,
    save = vim.empty_dict()
  },
  typeDefinitionProvider = true,
  workspace = {
    workspaceFolders = {
      changeNotifications = "workspace/didChangeWorkspaceFolders",
      supported = true
    }
  },
  workspaceSymbolProvider = true
}
]] --

local lsp_highlight_document = function(client)
  if not client.server_capabilities.documentHighlightProvider then
    return
  end

  vim.api.nvim_exec([[
		augroup lsp_document_highlight
			autocmd! * <buffer>
			autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
			autocmd CursorHoldI <buffer> lua vim.lsp.buf.document_highlight()
			autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
			autocmd CursorMovedI <buffer> lua vim.lsp.buf.clear_references()
		augroup END
	]], false)
end

local lsp_format_on_write = function(client, bufnr)
  if not client.server_capabilities.documentFormattingProvider then
    return
  end

  vim.api.nvim_create_autocmd('BufWritePre', {
    desc = 'LSP document formatting on buf write',
    buffer = bufnr,
    callback = function(args)
      -- {
      --   buf = 1,
      --   event = "BufWritePre",
      --   file = "items/items.go",
      --   id = 33,
      --   match = "/home/asdfakl/dev/gitlab.com/asdfakl/gogame/items/items.go"
      -- }
      vim.lsp.buf.format({
        async = false,
        bufnr = args.buf,
        timeout_ms = 2000,
      })
    end,
  })
end

local lsp_keymaps = function(client, bufnr)
  local map = function(mode, lhs, callback)
    local opts = {
      noremap = true,
      silent = true,
      callback = callback,
    }
    vim.api.nvim_buf_set_keymap(bufnr, mode, lhs, '', opts)
  end

  local caps = client.server_capabilities

  if caps.definitionProvider then
    map('n', '<leader>-', vim.lsp.buf.definition)
  end

  if caps.hoverProvider then
    map('n', '<leader>n', vim.lsp.buf.hover)
  end

  map('n', '<leader>c', function()
    require('asdfakl.lsp.contextmenu').show(client)
  end)
end

M.setup = function()
  local config = {
    virtual_text = true,
    update_in_insert = false,
    underline = false,
    severity_sort = true,
    float = {
      focusable = false,
      style = 'minimal',
      border = 'rounded',
      source = 'always',
      header = '',
      prefix = '',
    },
  }

  vim.diagnostic.config(config)
end

M.on_attach = function(client, bufnr)
  lsp_highlight_document(client)

  lsp_format_on_write(client, bufnr)

  lsp_keymaps(client, bufnr)
end

local client_capabilities = vim.lsp.protocol.make_client_capabilities()

local cmp_capabilities = require('cmp_nvim_lsp').default_capabilities()

client_capabilities.textDocument = vim.tbl_extend(
  'force',
  client_capabilities.textDocument,
  cmp_capabilities.textDocument
)

M.capabilities = client_capabilities

return M
