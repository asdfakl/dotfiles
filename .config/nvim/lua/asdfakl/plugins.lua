--[[
Clone packer from https://github.com/wbthomason/packer.nvim
to vim.fn.stdpath 'data' .. '/site/pack/packer/start/packer.nvim'

print(vim.fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim')

After cloning run :packadd packer.nvim
and restart nvim.
]]
--

local dev_env = vim.env.DEV_ENV or 'home'

return require('packer').startup(function(use)
  use 'folke/tokyonight.nvim'
  use 'neovim/nvim-lspconfig'
  use 'nvim-lua/plenary.nvim'
  use 'nvim-telescope/telescope.nvim'
  use 'wbthomason/packer.nvim'

  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/nvim-cmp'
  use 'ray-x/cmp-treesitter'
  use 'saadparwaiz1/cmp_luasnip'

  use {
    'nvim-treesitter/nvim-treesitter',
    tag = 'v0.9.1',
    run = ':TSUpdate',
    config = function()
      require('nvim-treesitter.configs').setup({
        ensure_installed = {
          'bash',
          'go',
          'html',
          'javascript',
          'json',
          'lua',
          'python',
          'terraform',
        },
        highlight = {
          enable = true,
          -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
          -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
          -- Using this option may slow down your editor, and you may see some duplicate highlights.
          -- Instead of true it can also be a list of languages
          additional_vim_regex_highlighting = false,
        },
        incremental_selection = {
          enable = false,
          keymaps = {
            init_selection = '<leader>q',
            node_incremental = '<leader>k',
            scope_incremental = '<leader>s',
            node_decremental = '<leader>j',
          },
        },
      })
    end,
  }

  use {
    'phaazon/hop.nvim',
    branch = 'v2', -- optional but strongly recommended
    config = function()
      require('hop').setup({
        keys = 'jkhlgfdsauiyotprewqnmb,vc-xz<\'', -- do not include dot in keys, see https://github.com/phaazon/hop.nvim/issues/263
        jump_on_sole_occurrence = true,
        create_hl_autocmd = true,
        uppercase_labels = false,
        multi_windows = false,
      })
    end,
  }

  use {
    'l3mon4d3/luasnip',
    config = function()
      require('luasnip').setup({
        history = false,
        update_events = 'TextChanged,TextChangedI',
        ext_base_prio = 300,
        ext_prio_increase = 1,
      })
    end,
  }

  use {
    'https://gitlab.com/asdfakl/reg-exec.nvim.git',
    branch = 'next',
  }

  use {
    'jose-elias-alvarez/null-ls.nvim',
    config = function()
      local null_ls = require('null-ls')

      local sources = {
        null_ls.builtins.diagnostics.shellcheck,
        null_ls.builtins.diagnostics.golangci_lint.with({
          args = function(params)
            -- defaults: { 'run', '--fix=false', '--fast', '--out-format=json', '$DIRNAME', '--path-prefix', '$ROOT' }
            return {
              'run',
              '--fix=false',
              '--out-format=json',
              '$DIRNAME',
              '--path-prefix',
              require('null-ls.utils').root_pattern('go.mod')(vim.fn.getcwd()) or params.root,
            }
          end,
          cwd = function(params)
            return require('null-ls.utils').root_pattern('go.mod')(vim.fn.getcwd()) or params.root
          end,
        }),
      }

      null_ls.setup({
        debounce = 2000,
        debug = false,
        sources = sources,
      })
    end,
  }

end)
