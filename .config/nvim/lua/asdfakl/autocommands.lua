vim.cmd [[autocmd filetype netrw nnoremap <buffer> - <NOP>]]

local create_augroup = function(name)
  return vim.api.nvim_create_augroup(name, { clear = true })
end

local define_softtab_autocmd = function(group, filetypes)
  vim.api.nvim_create_autocmd('FileType', {
    group = group,
    pattern = filetypes,
    callback = function(args)
      vim.api.nvim_buf_set_option(args.buf, 'tabstop', 2)
      vim.api.nvim_buf_set_option(args.buf, 'shiftwidth', 2)
      vim.api.nvim_buf_set_option(args.buf, 'softtabstop', 2)
      vim.api.nvim_buf_set_option(args.buf, 'expandtab', true)
    end,
  })
end

local define_block_movement_autocmd = function(group, filetypes)
  vim.api.nvim_create_autocmd('FileType', {
    group = group,
    pattern = filetypes,
    callback = function(args)
      vim.api.nvim_buf_set_keymap(args.buf, 'n', '¨', '[{', { noremap = true })
      vim.api.nvim_buf_set_keymap(args.buf, 'n', '~', ']}', { noremap = true })
    end
  })
end

define_softtab_autocmd(create_augroup('softtab_aug'), {
  'terraform',
  'sql',
  'lua',
  'html',
  'json',
  'javascript',
})

define_block_movement_autocmd(create_augroup('block_movement_aug'), {
  'go',
  'rust',
})

local fold_group = create_augroup 'fold_aug'
vim.api.nvim_create_autocmd('FileType', {
  group = fold_group,
  pattern = { 'text' },
  callback = function()
    vim.api.nvim_win_set_option(0, 'foldmethod', 'marker')
  end
})
