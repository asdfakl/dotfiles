local ls = require('luasnip')
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node

local function cp(args)
  return args[1]
end

local sh_snips = {}

sh_snips[#sh_snips + 1] = s("sh", {
  t("#!/bin/bash")
})

local go_snips = {}

go_snips[#go_snips + 1] = s("ife", {
  t("if "), i(1, "err"), t(" != nil {"),
  t({ "", "\treturn " }), f(cp, { 1 }), i(0),
  t({ "", "}" }),
})

ls.add_snippets("sh", sh_snips)
ls.add_snippets("go", go_snips)
