local cmp = require 'cmp'
local types = require 'cmp.types'
local mapping = require 'cmp.config.mapping'
local luasnip = require 'luasnip'

require("luasnip/loaders/from_vscode").lazy_load()

local sources = {
  { name = 'luasnip' },
  {
    name = 'nvim_lsp',
    keyword_length = 4,
  },
  { name = 'treesitter' },
  {
    name = 'buffer',
    keyword_length = 3,
    option = {
      get_bufnrs = function()
        return vim.api.nvim_list_bufs()
      end
    },
  },
}

cmp.setup({
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  mapping = {
        ['<Down>'] = { i = mapping.select_next_item({ behavior = types.cmp.SelectBehavior.Select }) },
        ['<Up>'] = { i = mapping.select_prev_item({ behavior = types.cmp.SelectBehavior.Select }) },
        ['<C-j>'] = { i = mapping.select_next_item({ behavior = types.cmp.SelectBehavior.Insert }) },
        ['<C-k>'] = { i = mapping.select_prev_item({ behavior = types.cmp.SelectBehavior.Insert }) },
        ['<cr>'] = { i = mapping.confirm({ select = true }) },
        ['<C-e>'] = { i = mapping.abort() },
  },
  formatting = {
    fields = { "kind", "abbr", "menu" },
    format = function(entry, vim_item)
      vim_item.menu = ({
            luasnip = '[SNP]',
            treesitter = '[TS]',
            buffer = '[BUF]',
            nvim_lsp = '[LSP]'
          })[entry.source.name] or entry.source.name

      return vim_item
    end,
  },
  sources = sources,
  experimental = {
    ghost_text = true,
  },
})
