local pickers = require 'telescope.pickers'
local finders = require 'telescope.finders'
local sorters = require 'telescope.sorters'
local previewers = require 'telescope.previewers'
local actions_set = require 'telescope.actions.set'
local actions_state = require 'telescope.actions.state'
local actions = require 'telescope.actions'
local from_entry = require 'telescope.from_entry'
local builtin = require 'telescope.builtin'

local dev_home = vim.env.DEV_HOME or vim.env.HOME
assert(dev_home, 'environment variable DEV_HOME or HOME required')

local pick_repo = function(callback)
  pickers.new {
    results_title = 'Repositories',
    -- Run an external command and show the results in the finder window
    finder = finders.new_oneshot_job({ 'locaterepos', dev_home }),
    sorter = sorters.get_fuzzy_file(),
    previewer = previewers.new_termopen_previewer {
      -- Execute another command using the highlighted entry
      get_command = function(entry)
        return { 'displayrepo', entry.value }
      end
    },
    attach_mappings = function(prompt_bufnr)
      actions_set.select:replace(function()
        local entry = actions_state.get_selected_entry()
        actions.close(prompt_bufnr)
        local repo = from_entry.path(entry)
        if repo then
          callback(repo)
        end
      end)
      return true
    end,
  }:find()
end

local M = {}

M.repo_find_files = function()
  pick_repo(function(repo)
    builtin.git_files({ cwd = repo })
  end)
end

M.repo_explore = function()
  pick_repo(function(repo)
    vim.api.nvim_command('lcd ' .. repo)
    vim.api.nvim_command('Explore' .. repo)
  end)
end

return M
