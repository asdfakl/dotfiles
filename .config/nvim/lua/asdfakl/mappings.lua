local map = function(mode, lhs, rhs, opts)
  local options = { noremap = true }
  if opts then
    options = vim.tbl_extend('force', options, opts)
  end
  vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

local ls_expand_or_jump_forward = function()
  local ls = require 'luasnip'

  if not ls.expand_or_locally_jumpable() then
    return
  end

  ls.expand_or_jump()
end

local ls_jump_backward = function()
  local ls = require 'luasnip'

  if not ls.jumpable(-1) then
    return
  end

  ls.jump(-1)
end

-- insert mode
map('i', '<C-n>', '<NOP>')
map('i', '<C-p>', '<NOP>')
map('i', 'jk', '<Esc>')
map('i', '<C-j>', '<C-n>')
map('i', '<C-k>', '<C-p>')
map('i', '<C-n>', '', { callback = ls_expand_or_jump_forward })
map('i', '<C-p>', '', { callback = ls_jump_backward })

-- select mode
map('s', '<C-n>', '', { callback = ls_expand_or_jump_forward })
map('s', '<C-p>', '', { callback = ls_jump_backward })

-- terminal mode
map('t', '<Esc>', '<C-\\><C-n>')
map('t', 'jk', '<C-\\><C-n>')

-- command mode
map('c', '<C-n>', '<NOP>')
map('c', '<C-p>', '<NOP>')
map('c', '<C-j>', '<C-n>')
map('c', '<C-k>', '<C-p>')

-- normal mode
map('n', '<Up>', '<NOP>')
map('n', '<Down>', '<NOP>')
map('n', '<Left>', '<NOP>')
map('n', '<Right>', '<NOP>')
map('n', 'å', '{')
map('n', 'ä', '}')
map('n', ';', ',')
map('n', ',', ';')
map('n', '\'', '`')
map('n', '<leader>h', '<C-w>h')
map('n', '<leader>l', '<C-w>l')
map('n', '<leader>j', '<C-w>j')
map('n', '<leader>k', '<C-w>k')
map('n', '<leader>s', [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/g<Left><Left>]])
map('n', '<leader>z', ':nohlsearch<cr>', { silent = true })
map('n', '<leader>e', ':Lex 15<cr>', { silent = true })
map('n', '<leader>m', ':bnext<cr>', { silent = true })
map('n', '<space>q', ':HopPattern<cr>', { silent = true })
map('n', '<space>s', ':HopChar1<cr>', { silent = true })
map('n', '<space>w', ':HopWord<cr>', { silent = true })
map('n', '<space>wk', ':HopWordBC<cr>', { silent = true })
map('n', '<space>wj', ':HopWordAC<cr>', { silent = true })
map('n', '<space>k', ':HopLineStartBC<cr>', { silent = true })
map('n', '<space>j', ':HopLineStartAC<cr>', { silent = true })
map('n', '<leader>ff', ':Telescope git_files<cr>', { silent = true })
map('n', '<leader>fg', ':Telescope live_grep<cr>', { silent = true })
map('n', '<leader>b', ':Telescope buffers<cr>', { silent = true })
map('n', '<leader>fb', ':Telescope current_buffer_fuzzy_find sorting_strategy=ascending<cr>', { silent = true })
map('n', '<leader>ft', ':Telescope treesitter<cr>', { silent = true })
map('n', '<leader>D', '', { callback = vim.diagnostic.goto_next })
map('n', '<leader>rff', '', {
  callback = function()
    require('asdfakl.misc').repo_find_files()
  end,
})
map('n', '<leader>re', '', {
  callback = function()
    require('asdfakl.misc').repo_explore()
  end,
})

-- visual mode
map('v', 'å', '{')
map('v', 'ä', '}')
map('v', ';', ',')
map('v', ',', ';')
map('v', '<', '<gv')
map('v', '>', '>gv')
map('v', '<F5>', [[y<cmd>lua require'reg-exec'.exec_register_async_by_telescope()<cr>]], { silent = true })
