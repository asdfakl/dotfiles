-- general options
vim.opt.completeopt    = 'menu,longest,preview'
vim.opt.expandtab      = false
vim.opt.ignorecase     = true
vim.opt.inccommand     = 'nosplit' -- When nonempty, shows the effects of |:substitute|, |:smagic|, and |:snomagic| as you type. Possible values: nosplit Shows the effects of a command incrementally in the buffer. split Like "nosplit", but also shows partial off-screen results in a preview window.
vim.opt.incsearch      = true
vim.opt.laststatus     = 1 -- The value of this option influences when the last window will have a status line: 0: never 1: only if there are at least two windows 2: always 3: always and ONLY the last window
vim.opt.mouse          = '' -- Disable mouse
vim.opt.number         = true
vim.opt.path           = '.,,'
vim.opt.pumheight      = 10 -- Maximum number of items to show in the popup menu (|ins-completion-menu|). Zero means "use available screen space".
vim.opt.relativenumber = true
vim.opt.scrolloff      = 3 -- Minimal number of screen lines to keep above and below the cursor.
vim.opt.shiftwidth     = 4
vim.opt.showcmd        = true
vim.opt.smartcase      = true
vim.opt.smartindent    = true
vim.opt.tabstop        = 4
vim.opt.termguicolors  = true -- Enables 24-bit RGB color in the |TUI|.
vim.opt.updatetime     = 2000
vim.opt.wildmenu       = true
vim.opt.wildmode       = 'longest:full,list'
vim.opt.wrap           = false

-- appendable options
vim.opt.iskeyword:append '-' -- treat '-' as part of word
vim.opt.wildignore:append '**/.git/**'

-- global settings
vim.g.loaded_node_provider    = 0
vim.g.loaded_perl_provider    = 0
vim.g.loaded_python3_provider = 0
vim.g.loaded_ruby_provider    = 0
vim.g.mapleader               = '-'
vim.g.maplocalleader          = '-'
vim.g.netrw_banner            = 0
vim.g.netrw_list_hide         = [[\.git/$]]
vim.g.netrw_liststyle         = 3

-- commands
vim.cmd [[command! Qa qa]]
vim.cmd [[command! Q q]]
vim.cmd [[command! Wq wq]]
vim.cmd [[command! W w]]
vim.cmd [[command! -range Bash lua require'reg-exec'.exec_range_async_by_key('bash', <line1>, <line2>)]]
