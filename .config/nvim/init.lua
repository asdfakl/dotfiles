require 'asdfakl.options'
require 'asdfakl.mappings'
require 'asdfakl.autocommands'
require 'asdfakl.plugins'
require 'asdfakl.colorscheme'
require 'asdfakl.snippets'
require 'asdfakl.cmp'
require 'asdfakl.lsp'
require 'asdfakl.misc'

local status_ok, _ = pcall(require, 'asdfakl.local')
if not status_ok then
  vim.notify 'lua module asdfakl.local not found'
end
