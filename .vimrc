syntax on
set autoindent
set backspace=2
set completeopt=menu,longest,preview
set foldmethod=marker
set hidden
set hlsearch
set ignorecase
set incsearch
set laststatus=2
set linebreak
set nocompatible
set noerrorbells
set nostartofline
set noswapfile
set nowrap
set number relativenumber
set shiftwidth=4
set showcmd
set smartcase
set tabstop=4
set t_vb=
set visualbell
set wildmenu
set wildmode=longest:full,list

set path=$PWD/**
colorscheme desert
let mapleader = "-"

"mappings
mapclear | mapclear <buffer> | mapclear! | mapclear! <buffer>

inoremap jk <Esc>
inoremap <C-j> <C-n>
inoremap <C-k> <C-p>

cnoremap <C-j> <C-n>
cnoremap <C-k> <C-p>

nnoremap å {
nnoremap ä }
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
nnoremap ; ,
nnoremap , ;
nnoremap ' `
nnoremap <leader>h <C-w>h
nnoremap <leader>l <C-w>l
nnoremap <leader>j <C-w>j
nnoremap <leader>k <C-w>k
nnoremap <leader>s :%s/\<<C-r><C-w>\>/<C-r><C-w>/g<left><left>
nnoremap <leader>b :ls<cr>:b<space>
nnoremap <leader>f :find<space>
nnoremap <silent> <leader>z :nohlsearch<cr>

vnoremap å {
vnoremap ä }
vnoremap ; ,
vnoremap , ;

"aliases
command! W w
command! Wq wq
command! Q q
command! Qa qa
